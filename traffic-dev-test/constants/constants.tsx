import {
  validateFirstname,
  validateLastname,
  validateEmail,
  validatePhone,
} from '../validators/validators'

export const MENU_ITEMS = [
  {
    text: 'VISIT OUR SALES CENTRE',
    icon: '/Shape.png',
  },
  {
    text: '1300 354 786',
    icon: '/Path.png',
  },
]
export const FORM_INPUT_ELEMENTS = [
  {
    id: 'firstname',
    name: 'firstname',
    placeholder: 'First Name',
    type: 'text',
    label: 'First Name',
    testId: 'first-name',
    autoComplete: 'given-name',
    required: true,
    validate: validateFirstname,
  },
  {
    id: 'lastname',
    name: 'lastname',
    placeholder: 'Last Name',
    type: 'text',
    label: 'Last Name',
    testId: 'last-name',
    autoComplete: 'family-name',
    required: true,
    validate: validateLastname,
  },
  {
    id: 'email',
    name: 'email',
    placeholder: 'Email',
    type: 'email',
    label: 'Email',
    testId: 'email',
    autoComplete: 'email',
    required: true,
    validate: validateEmail,
  },
  {
    id: 'phone',
    name: 'phone',
    placeholder: 'Phone',
    type: 'text',
    label: 'Phone',
    testId: 'phone',
    autoComplete: 'tel',
    required: true,
    validate: validatePhone,
  },
]
export const FORM_OPTIONS = [
  {
    value: 'apartments',
    option: 'Apartment',
  },
  {
    value: 'forRent',
    option: 'For Rent',
  },
  {
    value: 'office',
    option: 'Office',
  },
]
export const FORM_CHECKBOX_ELEMENTS = [
  {
    id: 'value1',
    name: 'customerChoice',
    label: 'Value 1',
    testId: 'value-1',
  },
  {
    id: 'value2',
    name: 'customerChoice',
    label: 'Value 2',
    testId: 'value-2',
  },
  {
    id: 'value3',
    name: 'customerChoice',
    label: 'Value 3',
    testId: 'value-3',
  },
  {
    id: 'value4',
    name: 'customerChoice',
    label: 'Value 4',
    testId: 'value-4',
  },
  {
    id: 'value5',
    name: 'customerChoice',
    label: 'Value 5',
    testId: 'value-5',
  },
  {
    id: 'value6',
    name: 'customerChoice',
    label: 'Value 6',
    testId: 'value-6',
  },
  {
    id: 'value7',
    name: 'customerChoice',
    label: 'Value 7',
    testId: 'value-7',
  },
]

export const fadeImages = [
  'backgroundImage.png',
  'https://images.unsplash.com/photo-1509721434272-b79147e0e708?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&h=896&q=80',
  'backgroundImage.png',
]
