import Head from 'next/head'
import Navigation from '../components/Navigation/Navigation'
import FormComponent from '../components/FormComponent/FormComponent'
import SliderComponent from '../components/SliderComponent/SliderComponent'

const Home = (): JSX.Element => {
  return (
    <div>
      <Head>
        <title>Traffic App - Dev Test</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="container">
        <Navigation />
        <SliderComponent />
        <FormComponent />
      </div>

      <style jsx>{`
        .container {
          position: relative;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          width: 100%;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        a {
          text-decoration: none;
          color: black;
        }

        ul {
          list-style: none;
          color: black;
        }

        * {
          box-sizing: border-box;
        }

        select {
          -webkit-appearance: none;
          -moz-appearance: none;
          appearance: none;
        }

        select::-ms-expand {
          display: none;
        }
      `}</style>
    </div>
  )
}
export default Home
