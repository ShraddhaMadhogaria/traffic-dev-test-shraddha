export const validateEmail = (value: string): string => {
  let error
  if (!value) {
    error = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = 'Invalid email address'
  }
  return error
}
export const validateFirstname = (value: string): string => {
  let error
  if (!value) {
    error = 'Required'
  }
  return error
}
export const validateLastname = (value: string): string => {
  let error
  if (!value) {
    error = 'Required'
  }
  return error
}
export const validatePhone = (value: string): string => {
  let error
  if (!value) {
    error = 'Required'
  }
  return error
}
