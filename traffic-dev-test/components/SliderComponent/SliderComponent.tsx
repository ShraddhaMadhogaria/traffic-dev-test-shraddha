import React from 'react'
import 'react-slideshow-image/dist/styles.css'
import { Fade } from 'react-slideshow-image'
import styles from './SliderComponent.module.css'
import { fadeImages } from '../../constants/constants'

const PrevArrow = (): JSX.Element => (
  <div className={styles.prevArrow}>
    <button className={styles.sliderArrows}>
      <img src="/prev.svg" />
    </button>
  </div>
)

const NextArrow = (): JSX.Element => (
  <div className={styles.nextArrow}>
    <button className={styles.sliderArrows}>
      <img src="/next.svg" />
    </button>
  </div>
)

const SliderComponent = (): JSX.Element => {
  const properties = {
    duration: 5000,
    autoplay: true,
    transitionDuration: 500,
    infinite: true,
    prevArrow: PrevArrow(),
    nextArrow: NextArrow(),
  }

  return (
    <div className={`slide-container ${styles.sliderComp}`}>
      <Fade {...properties}>
        {fadeImages.map((each, index) => (
          <div key={index} className="each-fade">
            <img className="lazy" src={each} />
            <div>
              <p className={styles.copy}>
                Two stunning new townhome releases launching early 2021
              </p>
              <button className={styles.sliderMobileButton}>Register</button>
            </div>
          </div>
        ))}
      </Fade>
    </div>
  )
}

export default SliderComponent
