import React from 'react'
import styles from './Navigation.module.css'
import { MENU_ITEMS } from '../../constants/constants'

const Navigation: React.FC = (): JSX.Element => {
  return (
    <nav role="nav" className={styles.globalHeader}>
      <div className={styles.topHeader} />
      <div className={styles.logo}>
        <img
          src="/logo_desktop.png"
          alt="Logo"
          srcSet="/logo_mobile.png 320w, /logo_desktop.png 800w, /logo_desktop.png 1200w"
        />
      </div>
      <ul className={styles.menu}>
        {MENU_ITEMS.map((item) => (
          <li className={styles.menuItem} key={item.text}>
            <a href="#">
              <img className={styles.menuIcon} src={item.icon} />
              <span className={styles.menuText}>{item.text}</span>
            </a>
          </li>
        ))}
      </ul>
    </nav>
  )
}
export default Navigation
