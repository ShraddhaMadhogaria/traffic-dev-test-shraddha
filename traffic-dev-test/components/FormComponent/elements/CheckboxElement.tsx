import React from 'react'
import { Field } from 'formik'
import styles from './FormElements.module.css'
interface ICheckbox {
  id: string
  name: string
  label: string
  testId: string
}

const CheckboxElement = ({
  id,
  name,
  label,
  testId,
}: ICheckbox): JSX.Element => {
  return (
    <div
      role="group"
      key={id}
      data-testid={`${testId}-div`}
      className={styles.checkboxElement}
    >
      <Field
        type="checkbox"
        name={name}
        value={id}
        data-testid={`${testId}-checkbox`}
      />
      <label htmlFor={name}>{label}</label>
    </div>
  )
}
export default CheckboxElement
