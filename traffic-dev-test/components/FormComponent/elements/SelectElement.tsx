import React from 'react'
import { Field } from 'formik'
import styles from './FormElements.module.css'

interface IOption {
  value: string
  option: string
}
interface ISelect {
  name: string
  options: IOption[]
  testId: string
}

const SelectElement = ({ name, options, testId }: ISelect): JSX.Element => {
  return (
    <div data-testid={`${testId}-div`} className={styles.selectContainer}>
      <Field
        name={name}
        as="select"
        className={styles.formElement}
        data-testid={`${testId}-select`}
      >
        <option value="">What are you looking for?</option>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.option}
          </option>
        ))}
      </Field>
    </div>
  )
}

export default SelectElement
