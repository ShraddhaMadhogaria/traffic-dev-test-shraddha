import React from 'react'
import styles from './FormElements.module.css'
import { Field } from 'formik'
interface IInput {
  name: string
  id: string
  placeholder: string
  testId: string
  type: string
  autoComplete: string
  required: boolean
  validate: (values: string) => string
}

const InputElement = ({
  name,
  id,
  placeholder,
  testId,
  type,
  autoComplete,
  required,
  validate,
}: IInput): JSX.Element => {
  return (
    <Field name={name} validate={validate}>
      {({ field, form: { touched, errors } }: any) => {
        const isError = !!(touched[name] && errors[name])
        return (
          <div
            data-testid={`${testId}-div`}
            className={styles.formElementWrapper}
          >
            <input
              {...field}
              type={type}
              id={id}
              autoComplete={autoComplete}
              data-testid={`${testId}-input`}
              placeholder={placeholder}
              className={styles.formElement}
              required={required}
            />
            <div className={styles.errorCopy}>{isError && errors[name]}</div>
          </div>
        )
      }}
    </Field>
  )
}

export default InputElement
