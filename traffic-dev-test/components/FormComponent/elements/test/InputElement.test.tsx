import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent, cleanup, act } from '@testing-library/react'
import { Formik, Form } from 'formik'
import InputElement from '../InputElement'
import { FORM_INPUT_ELEMENTS } from '../../../../constants/constants'
import FormikPersist from '../../FormikPersistor'
describe('<InputElement />', () => {
  const props = FORM_INPUT_ELEMENTS[0]
  const formName = 'ServiceFormPersist'
  const storageKey = `formik.form.${formName}`
  let inputElement
  beforeEach(() => {
    inputElement = render(
      <Formik
        initialValues={{
          [props.name]: '',
        }}
        onSubmit={() => null}
      >
        {() => (
          <Form role="form">
            <FormikPersist name={formName} />
            <InputElement {...props} />
          </Form>
        )}
      </Formik>
    )
  })

  afterEach(cleanup)

  describe('when changed', () => {
    it('expect session storage for input element to have a value', () => {
      const input = inputElement.getByTestId(`${props.testId}-input`)

      act(() => {
        fireEvent.change(input, { target: { value: 'Sam' } })
      })
      act(() => {
        const storageValue = JSON.parse(sessionStorage.getItem(storageKey))
        expect(storageValue.values[`${props.name}`]).toBe('Sam')
      })
    })
  })
})
