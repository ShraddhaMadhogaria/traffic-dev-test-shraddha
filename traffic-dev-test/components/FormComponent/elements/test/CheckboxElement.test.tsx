import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent, cleanup, act } from '@testing-library/react'
import { Formik, Form } from 'formik'
import CheckboxElement from '../CheckboxElement'
import { FORM_CHECKBOX_ELEMENTS } from '../../../../constants/constants'
import FormikPersist from '../../FormikPersistor'
describe('<Checkbox Input />', () => {
  const props = FORM_CHECKBOX_ELEMENTS[0]
  const formName = 'ServiceFormPersist'
  const storageKey = `formik.form.${formName}`
  let inputCheckbox
  beforeEach(() => {
    inputCheckbox = render(
      <Formik
        initialValues={{
          customerChoice: [],
        }}
        onSubmit={() => null}
      >
        {() => (
          <Form role="form">
            <FormikPersist name={formName} />
            <CheckboxElement {...props} />
          </Form>
        )}
      </Formik>
    )
  })

  afterEach(cleanup)

  describe('when changed', () => {
    it('expect the checked to be true, and session storage has value', () => {
      const checkbox = inputCheckbox.getByTestId(`${props.testId}-checkbox`)
      act(() => {
        fireEvent.click(checkbox)
      })
      act(() => {
        expect(checkbox.checked).toEqual(true)
        const storageValue = JSON.parse(sessionStorage.getItem(storageKey))
        expect(storageValue.values[`customerChoice`]).toEqual(
          JSON.parse('["value1"]')
        )
      })
    })
  })
})
