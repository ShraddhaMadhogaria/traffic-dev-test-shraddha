import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent, cleanup, act } from '@testing-library/react'
import { Formik, Form } from 'formik'
import SelectElement from '../SelectElement'
import { FORM_OPTIONS } from '../../../../constants/constants'
import FormikPersist from '../../FormikPersistor'
describe('<SelectElement />', () => {
  let selectElement
  const formName = 'ServiceFormPersist'
  const storageKey = `formik.form.${formName}`
  const props = {
    options: FORM_OPTIONS,
    name: 'service',
    testId: 'services',
  }
  beforeEach(() => {
    selectElement = render(
      <Formik
        initialValues={{
          service: '',
        }}
        onSubmit={() => null}
      >
        {() => (
          <Form role="form">
            <FormikPersist name={formName} />
            <SelectElement {...props} />
          </Form>
        )}
      </Formik>
    )
  })

  afterEach(cleanup)

  describe('when changed', () => {
    it('expect session storage for select element to have a value selected', () => {
      const input = selectElement.getByTestId(`${props.testId}-select`)
      act(() => {
        fireEvent.change(input, { target: { value: props.options[0].value } })
      })
      act(() => {
        const storageValue = JSON.parse(sessionStorage.getItem(storageKey))
        expect(storageValue.values[`${props.name}`]).toBe(
          props.options[0].value
        )
      })
    })
  })
})
