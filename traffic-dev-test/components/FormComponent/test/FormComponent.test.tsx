import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent, act, render } from '@testing-library/react'
import FormComponent from '../FormComponent'
import { FORM_CHECKBOX_ELEMENTS } from '../../../constants/constants'
describe('<FormComponent />', () => {
  const container = () => {
    return render(<FormComponent />)
  }

  afterEach(() => {
    cleanup()
  })

  it('Should have submit button disabled when checkboxes are not clicked', () => {
    const { getByTestId } = container()
    const inputFirstname = getByTestId(`first-name-input`)
    const inputLastname = getByTestId(`last-name-input`)
    const inputEmail = getByTestId(`email-input`)
    const inputPhone = getByTestId(`phone-input`)
    const selectElement = getByTestId(`services-select`)
    const button = (getByTestId(
      'submit-button'
    ) as unknown) as HTMLButtonElement
    act(() => {
      fireEvent.change(inputFirstname, { target: { value: 'Sam' } })
      fireEvent.change(inputLastname, { target: { value: 'M' } })
      fireEvent.change(inputEmail, { target: { value: 'shradzmm@gmail.com' } })
      fireEvent.change(inputPhone, { target: { value: '04000000000' } })
      fireEvent.change(selectElement, { target: { value: 'apartments' } })
    })

    expect(button.disabled).toEqual(true)
  })

  it('Should have submit button disabled when only one checkbox is clicked with an error message', () => {
    const { getByTestId, getByText } = container()
    const inputFirstname = getByTestId(`first-name-input`)
    const inputLastname = getByTestId(`last-name-input`)
    const inputEmail = getByTestId(`email-input`)
    const inputPhone = getByTestId(`phone-input`)
    const selectElement = getByTestId(`services-select`)
    const button = (getByTestId(
      'submit-button'
    ) as unknown) as HTMLButtonElement
    const inputCheckBoxOne = getByTestId(
      `${FORM_CHECKBOX_ELEMENTS[0].testId}-checkbox`
    )

    act(() => {
      fireEvent.change(inputFirstname, { target: { value: 'Sam' } })
      fireEvent.change(inputLastname, { target: { value: 'M' } })
      fireEvent.change(inputEmail, { target: { value: 'shradzmm@gmail.com' } })
      fireEvent.change(inputPhone, { target: { value: '04000000000' } })
      fireEvent.change(selectElement, { target: { value: 'apartments' } })
      fireEvent.click(inputCheckBoxOne)
    })

    expect(button.disabled).toEqual(true)
    expect(getByText('Please select 2 or more services')).toBeInTheDocument()
  })

  it('Should have submit button enabled when two or more checkbox is clicked', () => {
    const { getByTestId } = container()
    const inputFirstname = getByTestId(`first-name-input`)
    const inputLastname = getByTestId(`last-name-input`)
    const inputEmail = getByTestId(`email-input`)
    const inputPhone = getByTestId(`phone-input`)
    const selectElement = getByTestId(`services-select`)
    const button = (getByTestId(
      'submit-button'
    ) as unknown) as HTMLButtonElement
    const inputCheckBoxOne = getByTestId(
      `${FORM_CHECKBOX_ELEMENTS[0].testId}-checkbox`
    )
    const inputCheckBoxTwo = getByTestId(
      `${FORM_CHECKBOX_ELEMENTS[1].testId}-checkbox`
    )

    act(() => {
      fireEvent.change(inputFirstname, { target: { value: 'Sam' } })
      fireEvent.change(inputLastname, { target: { value: 'M' } })
      fireEvent.change(inputEmail, { target: { value: 'shradzmm@gmail.com' } })
      fireEvent.change(inputPhone, { target: { value: '04000000000' } })
      fireEvent.change(selectElement, { target: { value: 'apartments' } })
      fireEvent.click(inputCheckBoxOne)
      fireEvent.click(inputCheckBoxTwo)
    })

    expect(button.disabled).toEqual(false)
  })

  it('Should have submit button disabeled when 6 or more checkbox is clicked', () => {
    const { getByTestId } = container()
    const inputFirstname = getByTestId(`first-name-input`)
    const inputLastname = getByTestId(`last-name-input`)
    const inputEmail = getByTestId(`email-input`)
    const inputPhone = getByTestId(`phone-input`)
    const selectElement = getByTestId(`services-select`)
    const button = (getByTestId(
      'submit-button'
    ) as unknown) as HTMLButtonElement

    act(() => {
      fireEvent.change(inputFirstname, { target: { value: 'Sam' } })
      fireEvent.change(inputLastname, { target: { value: 'M' } })
      fireEvent.change(inputEmail, { target: { value: 'shradzmm@gmail.com' } })
      fireEvent.change(inputPhone, { target: { value: '04000000000' } })
      fireEvent.change(selectElement, { target: { value: 'apartments' } })
      FORM_CHECKBOX_ELEMENTS.map((element) => {
        fireEvent.click(getByTestId(`${element.testId}-checkbox`))
      })
    })

    expect(button.disabled).toEqual(false)
  })
})
