import React, { useState } from 'react'
import { Formik, Form } from 'formik'
import styles from './FormComponent.module.css'
import InputElement from './elements/InputElement'
import SelectElement from './elements/SelectElement'
import CheckboxElement from './elements/CheckboxElement'
import {
  FORM_INPUT_ELEMENTS,
  FORM_OPTIONS,
  FORM_CHECKBOX_ELEMENTS,
} from '../../constants/constants'
import { useForm } from '@formspree/react'
import FormikPersist from './FormikPersistor'

const FormComponent = (): JSX.Element => {
  const handleSubmit = useForm('xknkgnpk')[1]
  const [success, setSuccess] = useState(false)
  const initialValues = {
    firstname: '',
    lastname: '',
    email: '',
    phone: '',
    service: '',
    customerChoice: [],
  }

  const handleSubmitForm = (values, actions) => {
    try {
      handleSubmit(values).then(({ response }) => {
        if (response.ok) {
          setSuccess(true)
        }
      })
      actions.setSubmitting(true)
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err)
    }
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmitForm}
      enableReinitialize
      role="form"
    >
      {({ values: { customerChoice } }) => {
        return (
          <div className={styles.card}>
            <h3>
              Be the first to register for new townhome releases for first
              option
            </h3>
            <Form method="POST" role="form">
              <FormikPersist name="ServiceFormPersist" />
              {success && (
                <h2 className={styles.feedback}>Thank you for choosing us!</h2>
              )}
              {FORM_INPUT_ELEMENTS.map((element) => {
                return <InputElement key={element.id} {...element} />
              })}
              <SelectElement
                options={FORM_OPTIONS}
                name="service"
                testId="services"
              />
              <>
                {customerChoice.length !== 0 && customerChoice.length < 2 && (
                  <div className={styles.errorCopy}>
                    Please select 2 or more services
                  </div>
                )}
                {customerChoice.length >= 6 && (
                  <div className={styles.errorCopy}>
                    Please select 5 or fewer services
                  </div>
                )}
                {FORM_CHECKBOX_ELEMENTS.map((element) => {
                  return <CheckboxElement key={element.id} {...element} />
                })}
              </>

              <div className={styles.buttonWrapper}>
                <button
                  type="submit"
                  className={styles.submitButton}
                  disabled={
                    customerChoice.length < 2 || customerChoice.length >= 6
                  }
                  data-testid="submit-button"
                >
                  Submit
                </button>
                <button
                  type="reset"
                  className={styles.resetButton}
                  data-testid="reset-button"
                >
                  Reset
                </button>
              </div>
            </Form>
          </div>
        )
      }}
    </Formik>
  )
}

export default FormComponent
