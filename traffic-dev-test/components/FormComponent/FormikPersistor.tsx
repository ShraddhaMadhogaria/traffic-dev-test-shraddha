import React, { PureComponent } from 'react'
import { FormikConsumer } from 'formik'

type StringHashMapType = { [key: string]: string }

type FormikPersistorProps = {
  name: string
  values: StringHashMapType
  setValues: (values: StringHashMapType) => void
}

class FormikPersistor extends PureComponent<FormikPersistorProps> {
  props: FormikPersistorProps
  componentDidMount() {
    const { setValues } = this.props
    const data = sessionStorage.getItem(this.storageKey)
    if (data) {
      const { values } = JSON.parse(data)
      setValues(values)
    }
  }
  componentDidUpdate() {
    const { values } = this.props
    sessionStorage.setItem(this.storageKey, JSON.stringify({ values }))
  }

  get storageKey() {
    return `formik.form.${this.props.name}`
  }
  render() {
    return null
  }
}

const FormikPersist = ({ name }: { name: string }): JSX.Element => (
  <FormikConsumer>
    {({ values, setValues }) => (
      <FormikPersistor name={name} setValues={setValues} values={values} />
    )}
  </FormikConsumer>
)

export default FormikPersist
